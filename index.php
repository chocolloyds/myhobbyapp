<?php
session_start();
include("dbconnect.php");

$passerr_message = "";

if(!isset($_SESSION['username'])){
    if (isset($_POST['register'])) {
      $email = $_POST['email'];
      $username = $_POST['username'];
      $password = $_POST['password'];
      $repassword = $_POST['repassword'];

      if($password == $repassword){
        try{
          $sql = "INSERT INTO `users`(`email`, `username`, `password`) VALUES ('$email','$username', '$password')";
          // use exec() because no results are returned
          $conn->exec($sql);
          $_SESSION['username']=$username;
          header("location: home.php");
        }
        catch(PDOException $e)
            {
            echo  $e->getMessage();
            }

      }
      else {
        $passerr_message = "Passwords don't match";
      }
    }
}else {
  header("location: home.php");
}
$conn= null;

 ?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <title>Sign Up</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css">

    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>


  <div class="container">
      <div class="row">
          <div class="col-md-4 col-md-offset-4 col-xs-12">
              <div id="wrapper">
                <div class="register-header">
                    <h1>Register <span class="pull-right glyphicon glyphicon-pencil"></span></h1>
                    <p>Be a part of your hobby community</p>
                </div>
                <div class="well well-lg">
                    <form action="index.php" name="registerForm" id="registerForm" method="post">
                        <label for="email"><span class="glyphicon glyphicon-user"></span> Signup as</label>
                        <div class="form-group">
                            <input class="form-control" type="email" name='email' placeholder="Email Address">
                        </div>

                        <label for="username"><span class="glyphicon glyphicon-user"></span> Username</label>
                        <div class="form-group">
                            <input class="form-control" type="text" name='username' placeholder="Username">
                        </div>

                        <label for="password"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
                        <div class="form-group">
                            <input class="form-control" type="password" name='password' placeholder="Password">
                        </div>

                        <label for="rpassword"><span class="glyphicon glyphicon-eye-open"></span> Confirm Password</label>
                        <div class="form-group">
                            <input class="form-control" type="password" name='repassword' placeholder="Confirm Password">
                        </div>

                        <p class="text-danger bg-danger"><?php echo $passerr_message ?></p>

                        <button type="submit" name="register" class="btn btn-danger btn-block">Register</button>

                        <br>
                        <br>
                        <p class="text-left"><a href="login.php">Login</a></p>
                    </form>

                </div>
              </div>
          </div>
      </div>
  </div>


    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>

</html>
