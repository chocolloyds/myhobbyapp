<?php
session_start();
include("dbconnect.php");
$success_message= "";

  if (!isset($_SESSION['username'])) {
    header("location: login.php");
  }

  if(isset($_POST['save'])){
    $hobby = $_POST['hobby'];

    try{
      $sql = "INSERT INTO `hobbies`( `username`, `hobby`) VALUES ('".$_SESSION['username']."','$hobby')";
      // use exec() because no results are returned
      $conn->exec($sql);
      $success_message= "Hobby added successfully";
    }
    catch(PDOException $e)
        {
        echo  $e->getMessage();
        }
  }

 ?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Hobby</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css">

    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>


    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12">
                <div id="wrapper">
                    <div class="well well-sm">
                      <p class="pull-right"><a href="logout.php">Logout</a></p>

                        <h1>Hobbies:</h1>
                        <?php
                        $sql = "SELECT *  FROM `hobbies` where `username` = '".$_SESSION['username']."'";
                        $stmt = $conn->query($sql);
                        $stmt->execute();
                        $hobbies = $stmt->fetchAll();

                        foreach ($hobbies as $hobby) {
                          echo '<div class="panel panel-default">
                           <div class="panel-body">
                             '.$hobby['hobby'].'

                             <div class="pull-right">
                               <a href="#" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i></a>
                               <a href="delete_hobby.php?id='.$hobby['id'].'" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                             </div>

                           </div>
                         </div>';
                        }
                         ?>




                        <form class="form-inline" action="home.php" method="post">
                            <label for="hobby" class="text-success">Add Hobby</label>
                            <input type="text" name="hobby" value="" class="form-control">
                            <p class="text-success bg-success"></p>
                            <br>
                            <br>
                            <p class="text-right">
                                <button type="submit" name="save" class="btn btn-primary">SAVE</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>

</html>
