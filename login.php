<?php
session_start();
include("dbconnect.php");

$logerr_message = "";

if(!isset($_SESSION['username'])){
      if (isset($_POST['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];


        try{
          $sql = "SELECT  COUNT(*)  FROM `users` where `username` = '".$username."' AND `password` = '".$password."'";
          $stmt = $conn->query($sql);
          $stmt->execute();


          if ($stmt->fetchColumn() > 0) {
            $_SESSION['username']=$username;
            header("location: home.php");
          }
          else{
            $logerr_message = "Wrong username or password";
            }
          }
          catch(PDOException $e)
              {
              echo  $e->getMessage();
              }
    }
}
else {
  header("location: home.php");
}


$conn= null;

 ?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <title>Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/octicons/3.1.0/octicons.min.css">

    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-12">
                <div id="wrapper">
                    <div class="login-header">
                        <h1>Login <span class="pull-right glyphicon glyphicon-lock"></span></h1>

                    </div>
                    <div class="well well-lg">

                        <form action="login.php" name="loginForm" id="loginForm" method="post">
                            <h4 class="text-center">Welcome to my HobbyApp</h4>


                            <p class="text-danger bg-danger"><?php echo $logerr_message ?></p>

                            <label for="username"><span class="glyphicon glyphicon-user"></span> USN</label>
                            <div class="form-group">
                                <input class="form-control" type="text" name='username' placeholder="Username">
                            </div>

                            <label for="password"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
                            <div class="form-group">
                                <input class="form-control" type="password" name='password' placeholder="Password">
                            </div>



                            <button type="submit" class="btn btn-success btn-block" name="login">Sign In</button>
                            <br>
                            <br>
                            <p class="text-left"><a href="index.php">Sign Up</a></p>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>

</html>
